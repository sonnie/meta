Meta
====
This repo consists of a set of scripts maintained by the Debian Ruby team which
helps manage, update, build and test debian packages. [1]

Setup
=====
- Pre-requisites
    - It is highly recomended that you run the script on a Debian system. You
      can technically run this script on any debian based distribution and it
      will probably work most of the time. But there are bound to be errors in
      some of them where the distribution's policy regarding where something is
      installed or config files are kept or the package names differ from that
      of Debian.
    - Although you need a sid environment to pull updates for packages, you
      don't have to run
      [setup](https://salsa.debian.org/ruby-team/meta/-/blob/master/setup) on
      sid as sid is constantly changing and may contain bugs.
    - Make sure you have the apparmor package installed. If you're not sure,
      run `apt policy apparmor` to verify and install it if needed. [2]
    - Make sure you do not have other lxc-containers on your system as they may
      interfere with the setup process. If you do, then it would be better to
      destroy them before running
      [setup](https://salsa.debian.org/ruby-team/meta/-/blob/master/setup).
- Setting up
    - Clone this repo and run the
      [setup](https://salsa.debian.org/ruby-team/meta/-/blob/master/setup)
      script. Running the setup script from this repo on your Debian system
      will create all the required root file systems and install required
      packages.
    - Once the setup process is completed successfully you can run the scripts
      to automate various tasks. Details of the available scripts are listed in
      the next section. To run the scripts from anywhere on the system you can
      place the script in one of the `bin` folders or add the meta file to your
      path.

Contents
========
This repo contains several helper scripts for the Debian ruby team.

### Updating repositories

- `debhelper-compat` : 
  update debhelper compatibility level to a given version encoded in the script
- `new-upstream` : 
  download a new upstream version with uscan and commit changes
- `rewrite-gemwatch` : 
  move debian/watch to the gemwatch.debian.net service  
- `standards-version` : 
  update Standards-Version field from debian/control to newest policy version
- `vcs-salsa` : 
  update Vcs-* fields from debian/control from alioth to salsa

### Building and testing

- `build`, `test`, `upload`, `build-and-upload` : 
  build and/or run tests and autopkgtests and/or upload the built package

### Managing repositories

- `checkout` : 
  clone the repo of a project from salsa
- `make-projects-list` : 
  create the list of projects from the ruby team in salsa
- `new-package` :
  run gem2deb to start a new git repository and optionally pushes it
  to salsa with `setup-project`
- `setup-project` : 
  create a new project on salsa
- `webhook` : 
  set webhooks for a project on salsa according to parameters in lib/salsa.rb
- `update-mrconfig` : 
  run make-projects-list and update .mrconfig and .gitignore file according to
  the new list

Reference
=====================
- [1] : [ruby-team-meta-build : Debian Wiki](https://wiki.debian.org/Packaging/ruby-team-meta-build)
- [2] : [Autopkgtest Fail workaround](https://lists.debian.org/debian-ruby/2019/02/msg00035.html)
